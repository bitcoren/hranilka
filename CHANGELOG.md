# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.0.1] - 2022-05-09
### Added
- Initial commit

[Unreleased]: https://gitlab.com/bitcoren/hranilka/-/compare/v0.0.1...main
[0.0.1]: https://gitlab.com/bitcoren/hranilka/-/releases/v0.0.1
