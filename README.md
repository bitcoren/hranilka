# Hranilka

Hranilka - cloud storage of information / Хранилка - облачное хранилище информации

Warning:  
This is alpha software. Use at your own discretion!  
Much is missing or lacking polish. There are bugs.  
Not yet secure.  

### Requirements

Ubuntu 22.04, x86_64, 2 Core CPU, 4 GB RAM, 120 GB SSD, 10 Mbit/s WAN, Docker, WAN Ports TCP+UDP: 4001

### Installation

1. curl -L -o main.zip https://gitlab.com/bitcoren/hranilka/-/archive/main/hranilka-main.zip
2. unzip main.zip
3. cd hranilka-main
4. bash install.sh
5. The system will reboot
6. Add Apps

## Applications

1. FreshRss https://gitlab.com/bitcoren/app-freshrss-bitcoren
2. Web3.Storage https://gitlab.com/bitcoren/app-web3storage-bitcoren
3. Qbittorrent-nox https://gitlab.com/bitcoren/app-qbittorrent-bitcoren
