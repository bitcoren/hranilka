#!/usr/bin/env bash

cd $HRANILKA/bin/min
find $HRANILKA/bin/min -iname '*.sh' -exec bash {} \;

cd $HRANILKA/bin/30min
if [ $(date +%M) == "00" ] || [ $(date +%M) == "30" ]; then
find $HRANILKA/bin/30min -iname '*.sh' -exec bash {} \;
fi

cd $HRANILKA/bin/hour
if [ $(date +%M) == "05" ]; then
find $HRANILKA/bin/hour -iname '*.sh' -exec bash {} \;
fi

cd $HRANILKA/bin/day
if [ $(date +%M) == "10" ] && [ $(date +%H) == "00" ]; then
find $HRANILKA/bin/day -iname '*.sh' -exec bash {} \;
fi
