#!/usr/bin/env bash

python3 --version
pip3 --version
echo -e "NodeJS: $(node -v)"
echo -e "NPM: $(npm -v)"
ipfs --version
ipfs cat /ipfs/bafybeia5b6p6suxyvkdfvrpzwdfvgdlqxeefbjth57bkyattrbtnebonuu/test.txt
