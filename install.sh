#!/usr/bin/env bash

HRANILKA=$(pwd)
sudo apt update
sudo DEBIAN_FRONTEND=noninteractive apt -y full-upgrade
mkdir temp apps bin/min bin/5min bin/10min bin/30min bin/hour bin/6hours bin/12hours bin/day bin/week bin/month
export PATH="$PATH:$(pwd)/bin:/home/$USER/.local/bin"
echo PATH="$PATH:$(pwd)/bin:/home/$USER/.local/bin" | sudo tee /etc/environment
echo HRANILKA=$(pwd) | sudo tee -a /etc/environment
sudo DEBIAN_FRONTEND=noninteractive apt install -y p7zip-full python3-pip python3-venv tmux atop docker.io docker-compose
echo -e "export VISUAL=nano\nexport EDITOR=\"\$VISUAL\"" >> ~/.bashrc
sudo usermod -aG docker $USER

wget -O temp/ipfs.tar.gz https://github.com/ipfs/go-ipfs/releases/download/v0.13.0/go-ipfs_v0.13.0_linux-amd64.tar.gz
cd /usr/local/bin/; sudo tar -xf $HRANILKA/temp/ipfs.tar.gz go-ipfs/ipfs --strip-components=1; cd $HRANILKA
ipfs init --profile server
cp bin/ipfs.serv bin/ipfs.service
sed -i "s/User=ipfs/User=${USER}/g" bin/ipfs.service
sed -i "s/Group=ipfs/Group=${USER}/g" bin/ipfs.service
sudo mv bin/ipfs.service /etc/systemd/system/ipfs.service
sudo systemctl daemon-reload
sudo systemctl enable ipfs
sudo systemctl start ipfs

curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo DEBIAN_FRONTEND=noninteractive apt -y install nodejs
sudo npm update -g npm
sudo npm update -g

echo -n "$(date +%Z-%Y-%m-%d-%T) New Hranilka system: ">> $HRANILKA/hranilka.log
echo $(ipfs id | grep \"ID\" | sed "s/\"ID\": \"//g" | sed "s/\",//g") >> $HRANILKA/hranilka.log
ipfs name publish QmQPeNsJPyVWPFDVHb77w8G42Fvo15z4bG2X8D2GhfbSXc >> $HRANILKA/hranilka.log
crontab -l | { cat; echo -e "@reboot $HRANILKA/bin/boot.sh &\n*/1 * * * * $HRANILKA/bin/ticker.sh"; } | crontab -
rm -rf temp/*
sleep 9
sudo reboot
